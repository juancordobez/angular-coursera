import { Component, OnInit, Input, HostBinding, Output, EventEmitter} from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.models';

@Component({
  selector: 'app-saludador',
  templateUrl: './saludador.component.html',
  styleUrls: ['./saludador.component.css']
})
export class SaludadorComponent implements OnInit {
  @Input() destino!: DestinoViaje;
  @Input() position!: number;
  @HostBinding("attr.class") cssClass = "col-md-4";
  @Output() clicked: EventEmitter<DestinoViaje> = new EventEmitter ;
  constructor() {}

  ngOnInit(): void {
  }

  ir(){
    this.clicked.emit(this.destino);
    return false;
  }

}
