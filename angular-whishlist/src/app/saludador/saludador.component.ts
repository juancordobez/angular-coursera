import { Component, OnInit, Input, HostBinding, Output, EventEmitter} from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { VoteDownAction, VoteUpAction } from '../models/destino-viaje-state.model';
import { DestinoViaje } from '../models/destino-viaje.models';

@Component({
  selector: 'app-saludador',
  templateUrl: './saludador.component.html',
  styleUrls: ['./saludador.component.css']
})
export class SaludadorComponent implements OnInit {
  @Input() destino!: DestinoViaje;
  @Input() position!: number;
  @HostBinding("attr.class") cssClass = "col-md-4";
  @Output() onClicked: EventEmitter<DestinoViaje>;
  
  constructor(private store: Store<AppState>) {
    this.onClicked = new EventEmitter;
  }

  ngOnInit(): void {
  }

  ir(){
    this.onClicked.emit(this.destino);
    return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }

}
